'''Some trivial tests.
'''

from main import my_print

def test_print(capsys):
    """test case"""
    my_print("Hello, world!")
    out, _ = capsys.readouterr()
    assert out == "Hello, world!\n"
    